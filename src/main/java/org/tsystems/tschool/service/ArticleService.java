package org.tsystems.tschool.service;

import org.tsystems.tschool.dto.ArticleDto;
import org.tsystems.tschool.entity.Article;

import java.util.List;

/**
 * The interface Article to access and control Articles business logic.
 */
public interface ArticleService {

    /**
     * Find all articles.
     *
     * @return the list of all Articles
     */
    public List<ArticleDto> findAll();

    /**
     * Find article by id.
     *
     * @param id of article
     * @return the article dto
     */
    public Article findById(Long id);

    /**
     * Remove article by id
     *
     * @param id the id of article
     * @return the boolean if success
     */
    public void removeArticleById(Long id);

    /**
     * Save article
     *
     * @param articleDto article transfer object
     * @return the article transfer object
     */
    public Article saveArticle(ArticleDto articleDto);

    /**
     * Update article
     *
     * @param articleDto article transfer object
     * @return the article transfer object
     */
    public Article updateArticle(ArticleDto articleDto);
}

