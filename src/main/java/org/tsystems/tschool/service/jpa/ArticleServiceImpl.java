package org.tsystems.tschool.service.jpa;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.tsystems.tschool.dto.ArticleDto;
import org.tsystems.tschool.entity.Article;
import org.tsystems.tschool.service.ArticleService;

import java.util.Collections;
import java.util.List;

/**
 * Implementation of ArticleService interface
 */
@RequiredArgsConstructor
@Service
public class ArticleServiceImpl implements ArticleService {

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ArticleDto> findAll() {
        Article article = new Article();
        return Collections.EMPTY_LIST;
    }

    /**
     * {@inheritDoc}
     * @return
     */
    @Override
    public Article findById(Long id) {
        Article article = new Article();
        return article;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void removeArticleById(Long id) {

    }

    /**
     * {@inheritDoc}
     * @return
     */
    @Override
    public Article saveArticle(ArticleDto articleDto) {
        Article article = new Article();
        return article;
    }

    /**
     * {@inheritDoc}
     * @return
     */
    @Override
    public Article updateArticle(ArticleDto articleDto) {
        Article article = new Article();
        return article;
    }
}
