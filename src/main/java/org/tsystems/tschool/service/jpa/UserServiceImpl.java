package org.tsystems.tschool.service.jpa;

import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.tsystems.tschool.dto.UserDto;
import org.tsystems.tschool.entity.User;
import org.tsystems.tschool.service.UserService;

/**
 * Implementation of UserService interface
 */
@RequiredArgsConstructor
@Service
public class UserServiceImpl implements UserService {


    /**
     * {@inheritDoc}
     *
     * @return
     */
    @Override
    public User getUserByUsername(String username) {
        User user = new User();
        return user;
    }

    /**
     * {@inheritDoc}
     *
     * @return
     */
    @Override
    public User updateUser(UserDto userDto) {
        User user = new User();
        return user;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void updatePassword(String username, String password) {
    }
}
