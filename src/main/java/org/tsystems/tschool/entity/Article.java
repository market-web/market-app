package org.tsystems.tschool.entity;

import lombok.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * The class representing Article
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class Article implements Serializable {

    private Long id;
    private String title;
    private Float price;
    private Integer quantity;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Article)) return false;
        Article article = (Article) o;
        return getTitle().equals(article.getTitle());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getTitle());
    }
}
