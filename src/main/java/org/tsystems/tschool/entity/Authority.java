package org.tsystems.tschool.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.tsystems.tschool.enums.AuthorityType;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * Authority entity
 * Class to control user authorities
 */
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class Authority implements Serializable {

    private Long id;

    private AuthorityType role;

    private Set<User> users = new HashSet<>();
}