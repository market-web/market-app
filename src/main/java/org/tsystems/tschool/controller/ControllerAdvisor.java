package org.tsystems.tschool.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.tsystems.tschool.exception.ArticleAlreadyExistException;
import org.tsystems.tschool.exception.ItemNotFoundException;

@ControllerAdvice
public class ControllerAdvisor {

    @ExceptionHandler(ItemNotFoundException.class)
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public String articleNotFound() {
        return "Could not find this object, Please try again later";
    }

    @ExceptionHandler(ArticleAlreadyExistException.class)
    @ResponseStatus(value = HttpStatus.CONFLICT)
    public String createFailure() {
        return "alreadyExists";
    }
}