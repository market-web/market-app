package org.tsystems.tschool.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import org.tsystems.tschool.dto.UserDto;
import org.tsystems.tschool.service.UserService;

import javax.validation.Valid;
import javax.validation.constraints.Size;

@RequiredArgsConstructor
@RequestMapping("/user")
@RestController
public class UserController {

    final UserService userService;

    @PostMapping("/update/{username}")
    public String updateUser(@PathVariable("username") String username, @Valid @ModelAttribute("user") UserDto userDto) {
        userService.updateUser(userDto);
        return "updated user";
    }

    @PostMapping("/update/password")
    public String updateUserPassword(@RequestParam("password") @Size(min = 4, max = 20,
            message = "password's length should be between 4 and 20") String password, Authentication authentication) {
        userService.updatePassword(authentication.getName(), password);
        return "redirect:/user";
    }
}
