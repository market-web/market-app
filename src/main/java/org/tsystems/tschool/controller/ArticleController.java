package org.tsystems.tschool.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import org.tsystems.tschool.dto.ArticleDto;
import org.tsystems.tschool.service.ArticleService;

import javax.validation.Valid;

@RequiredArgsConstructor
@RestController
@RequestMapping("/articles")
public class ArticleController {

    private final ArticleService articleService;

    @PostMapping("/add")
    public String addArticle(@Valid ArticleDto articleDto) {

        articleService.saveArticle(articleDto);
        return "added";
    }

    @PostMapping("/edit/{id}")
    public String editArticle(@PathVariable Long id, ArticleDto articleDto) {
        articleService.updateArticle(articleDto);
        return "edited";
    }


    @GetMapping("/delete/{id}")
    public String deleteArticleById(@PathVariable Long id) {
        articleService.removeArticleById(id);
        return "deleted article";
    }
}
